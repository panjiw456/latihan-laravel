<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/author', 'AuthController@auth');
Route::post('/main', 'AuthController@send');

Route::get('/data-table' , function(){
    return view('halaman.datatable');
});

Route::get('/table' , function(){
    return view('halaman.table');
});

//CRUD Route

//create Data
//mengarahkan ke form tambah cast
Route::get('/cast/create', 'CastController@create');
//menyimpan data base
Route::post('/cast' , 'CastController@store');

//Read Data
//Route untuk menampilkan semua data base
Route::get('/cast', 'CastController@index'); 
//route detail cast
Route::get('/cast/(cast_id)' , 'CastController@show');