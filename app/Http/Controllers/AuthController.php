<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function auth()
    {
        return view('halaman.form');
    }

    public function send(Request $request)
    {
        //dd($request->all());
        $namaDepan = $request['FirstName'];
        $namaBelakang = $request['LastName']; 

        return view('halaman.main', compact('namaDepan','namaBelakang'));
    }
}
