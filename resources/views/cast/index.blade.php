@extends('layout.master')

@section('judul')
List Cast
@endsection

@section ('content')

<a href="/cast/create" class="btn btn-primary-3">Tambah Cast</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Biodata</th>
        <th scope="col">Umur</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data Tidak Ada</td>    
            </tr>
            
        @endforelse
    </tbody>
  </table>

@endsection