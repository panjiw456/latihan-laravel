@extends('layout.master')

@section('judul')
Halaman Home
@endsection

@section ('content')
<body>
    <h1>Selamat Datang, {{$namaDepan}} {{$namaBelakang}}</h1>
    <h3>Terima Kasih telah bergabung di Website Kami. Media belajar Kita!!</h3>
@endsection
</body>
</html>