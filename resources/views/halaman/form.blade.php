@extends('layout.master')

@section('judul')
Halaman Form Pendaftaran
@endsection

@section ('content')
<body>
    
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>

    <form action="/main" method="post">
        @csrf
        <label>First Name</label><br>
        <input type="text" name="FirstName"><br><br>

        <label>Last Name</label><br>
        <input type="text" name="LastName"><br><br>
        
        <label>Gender</label><br>
        <input type="radio" name="wn" value="male">Male<br>
        <input type="radio" name="wn" value="female">Female<br><br>

        <label>Nationality</label><br>
        <select name="nationality">
            <option value="1">Indonesian</option>
            <option value="2">Singaporean</option>
            <option value="3">Malaysian</option>
            <option value="4">Thai</option>
        </select><br><br>

        <label>Language Spoken</label><br>
        <input type="checkbox" value="1" name="language">Bahasa Indoneisa<br>
        <input type="checkbox" value="2" name="language">English<br>
        <input type="checkbox" value="3" name="language">Other
        <input type="text" name="Other"><br><br>

        <label>Bio</label><br>
        <textarea name="Bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
</body>
</html>